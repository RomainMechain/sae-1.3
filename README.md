# README SAE 1.3


## I) Choix du type d'installation :

Avant de commencer à installer Linux, il a fallu réfléchir à la méthode d'installation. Pour ma part j'ai choisi de faire une installation en «Dual Boot», c'est-à-dire de conserver Windows et de créer une partition sur laquelle sera Linux. Ainsi, à chaque lancement, il y aura possibilité de démarrer en Linux ou en Windows. Je pense qu'il s'agit de la meilleure solution car cela permet de reproduire presque identiquement l'espace de travail que l'on utilise durant les TP et en même temps de pouvoir continuer à utiliser Windows qui est plus pratique pour une utilisation quotidienne. 

## II) Préparation du matérielle nécessaire :

Maintenant, il faut préparer ce dont on a besoin pour l’installation, en effet, il faut réaliser plusieurs étapes en amont. Premièrement, créer une clé bootable, il s'agit d'une clé sur laquelle on pourra démarrer l'ordinateur. Pour cela on commence par télécharger l'image de ubuntu une instance Linux puis grâce à l'application «Rufus» on va formater la clé. Dans les paramètres de Rufus on va commencer par ajouter l'image préalablement installé, puis choisir le schéma de partition GPT et comme système de destination UEFI (non csm). Une fois la barre de chargement terminé, la clé est prête à l'emploi.

## III) Mise en place et démarrage sur la clé bootable : 

Cependant, il reste plusieurs protections à désactiver pour pouvoir installer Linux et celles-ci peuvent dépendre de la machine utilisé. Premièrement, dans les paramètres Windows, il faut taper dans la barre de recherche «bitlocker»afin de le désactiver(il s'agit d'un système permettant d'encoder les données). Une fois cela fait on peut redémarrer l'ordinateur en entrant dans le BIOS, pour ma part il faut rester appuyé sur f2 lors du démarrage de l'ordinateur. Une fois dedans il faut accéder au paramètres avancés,disponible en bas à droite de l’écran, puis aller dans l'onglet «Boot» pour désactiver le easy Boot puis le Boot secure dans l'onglet «Secure». Enfin on peut redémarrer l'ordinateur en branchant la clé et retourner dans le BIOS pour mettre la clé en première position de lancement. Pour cela, personnellement, il m'a fallu mettre la clé au-dessus de la liste des disques grâce à un cliqué/déposé, mais sur la plus part des ordinateurs il faut juste sélectionner la clé avec les flèches directionnelles, puis appuyer sur entré. On peut maintenant démarrer l’installation.


## IV) Finalisation de l'installation :

Lorsque que l'on redémarre l'ordinateur, on arrive sur un onglet on l'on peut choisir ubuntu, ce qui nous fera alors démarrer dessus, on peut alors choisir d'installer ubuntu, et de le faire à côté de Windows,et donc en dual-boot ce que l'on cherche à faire. On nous demande alors la quantité de mémoire que l'on veut y allouer afin de créer une partition, il est recommandé de faire une partition d'un minimum de vingt Gigabits, mais ayant assez de place et pour pouvoir faire toutes les installations nécessaires, j'ai préféré allouer cent Gigabits. Une fois celle-ci créé l'installation se finalise, il ne nous reste plus qu'a rentrer les informations demandés classique comme le nom d'utilisateur ou bien le mot de passe. L’installation est donc maintenant terminé et le dual-boot entre Windows et Linux opérationnelle. 

## V) Conclusion :

A chaque lancement, on peut maintenant choisir de démarrer soit sur Ubuntu soit sur Windows en sélectionnant soit l'un soit l'autre grâce à un menu.   


Lien vers le GitLab : https://gitlab.com/RomainMechain/sae-1.3 
