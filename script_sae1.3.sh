#!/bin/bash
# Script de la SAE 1.3 permetant l'instalation de tout ce qui est nécessaire
reponse=0
echo "Bonjour, grâce à ce script vous allez pouvoir effectuer plusieurs installations."
echo "Avant cela voulez vous mettre à jour la liste des paquets ? -1 pour Oui -2 pour Non"
read maj
if [ $maj -eq 1 ]; #vérifi si l'utilisateur souhaite mette à jour les paquets
 then sudo apt update # Mise à jour des paquets
fi
while [ $reponse -lt 5 ]; do #Boucle qui maintient le script en éxécution tant que l'utilisateur ne souhaite pas y mettre fin
	echo "Que voulez-vous installer ? Tappez le numéro correspondant."
	echo "-1 VSCode "
	echo "-2 java"
	echo "-3 python"
	echo "-4 Docker"
	echo "-5 Fin"
	read reponse
	 if [ $reponse -eq 1 ]; #vérifi si l'utilisateur veut installer VScode
	  then echo "Vous allez installer VSCode"
	       sudo apt install software-properties-common apt-transport-https wget -y
	       wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
	       sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" #ajoute VScode à la bibliothèque de paquets
	       sudo apt install code #Installe Vscode
	       echo "Voulez-vous installer GitLab avec ? -1 pour Oui -2 pour Non"
	       read i_git
	       if [ $i_git -eq 1 ]; #vérifi si l'utilisateur veut installer GitLab
		then sudo apt-get install git #installe GitLab
	       fi
	       echo "Voulez-vous installer les éxtensions avec ? -1 pour Oui -2 pour Non"
	       read i_ext
	       if [ $i_ext -eq 1 ]; #vérifi si l'utilisateur veut installer les éxtensions VScode
		then ./installation_extension.sh #Lance le script d'installation des éxtensions
	       fi
	 fi
	 if [ $reponse -eq 2 ]; #vérifi si l'utilisateur veut installer Java
	  then echo "Vous allez installer Java"
	       sudo apt install default-jre #installe Java runtime environment
	       sudo apt install default-jdk #installe Java development kit
	       echo "Voici le test de Java :"
	       javac test_java.java #compile le dossier test_java
	       java test_java #Execute le programme de test de Java
	 fi
	 if [ $reponse -eq 3 ]; #vérifi si l'utilisateur veut installer Python
	  then echo "Vous allez installer python"
	       sudo apt install python3 #installe Python
	       echo "Voici le test de python:"
	       python3 test_python.py #Execute le programme de test de Python
	 fi
	 if [ $reponse -eq 4 ]; #vérifi si l'utilisateur veut installer Docker
	  then echo "Vous allez installer Docker"
	       sudo apt install apt-transport-https ca-certificates curl software-properties-common curl
	       curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	       sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" #ajoute Docker à la bibliothèque de paquets
	       docker-engine docker.io
	       sudo apt install docker.io #installe Docker
	       echo "Voici le test de docker:"
	       sudo systemctl start docker #Permet de faire fonctionner Docker
	       sudo systemctl enable docker
	       sudo systemctl status docker
	       sudo docker run hello-world #Execute la commande de test de Docker 
	 fi
done
echo "Vous avez bien fermé le script"
